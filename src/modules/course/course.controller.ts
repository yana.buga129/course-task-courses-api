import {
  BadRequestException,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param,
} from '@nestjs/common';
import { CourseService } from './course.service';

interface CustomError extends Error {
  response?: {
    status?: number;
  };
}

function isCustomError(error: unknown): error is CustomError {
  return (error as CustomError).response?.status !== undefined;
}

@Controller('course')
export class CourseController {
  constructor(protected readonly service: CourseService) {}

  @Get(':code')
  public async getCourseData(@Param('code') courseCode: number): Promise<unknown> {
    require('https').globalAgent.options.rejectUnauthorized = false;

    try {
      const result = await this.service.getCourseData(courseCode);
      return result;
    } catch (error) {
      if (!isCustomError(error)) {
        throw new InternalServerErrorException('An unexpected error occurred');
      } else {
        let exception: any;
        // @ts-ignore
        exception = error.response.status;

        switch (exception) {
          case 404:
            throw new NotFoundException('Not found');
          case 400:
            throw new BadRequestException('Inserted values are incorrect');
          case 500:
            throw new InternalServerErrorException('Failed to connect to server');
          default:
            throw new InternalServerErrorException('An unexpected error occurred');
        }
      }
    }
  }
}
