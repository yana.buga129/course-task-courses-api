import { Injectable } from '@nestjs/common';
import { ICourse } from './types';
import axios from 'axios';
import * as cheerio from 'cheerio';
import { CourseSeason, EducationLevel } from '../../common/types';

@Injectable()
export class CourseService {
    async getCourseData(courseCode: number): Promise<ICourse> {
        const response = await axios.get(`https://my.ukma.edu.ua/course/${courseCode}`);

        const $ = cheerio.load(response.data);

        const code = Number($('#w0 > table > tbody:nth-child(1) > tr:nth-child(1) > td').text());
        const name = $('div.container > ul > li:nth-child(3)').text();
        const description = $(`#course-card--${courseCode}--info`).text().replace(/[\n\t]/g, '');
        const facultyName = $('#w0 > table > tbody:nth-child(1) > tr:nth-child(3) > td').text();
        const departmentName = $('#w0 > table > tbody:nth-child(1) > tr:nth-child(4) > td').text();
        const levelText = $('#w0 > table > tbody:nth-child(1) > tr:nth-child(5) > td').text();
        const level = levelText === 'Бакалавр' ? EducationLevel.BACHELOR : EducationLevel.MASTER;
        const year = Number(
            $('#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(3)').text().split(' ')[0]
        );
        const seasons: CourseSeason[] = [];

        $('#w0 > table > tbody:nth-child(2)').each((_number, element) => {
            const temp = $(element).find('th').text().split('\t');
            temp.forEach((t) => {
                if (t === 'Осінь') {
                    seasons.push(CourseSeason.SPRING);
                } else if (t === 'Весна') {
                    seasons.push(CourseSeason.AUTUMN);
                } else if (t === 'Літо') {
                    seasons.push(CourseSeason.SUMMER);
                }
            });
        });

        const creditsAmount = Number(
            $('#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(1)').text().split(' ')[0]
        );
        const hoursAmount = Number(
            $('#w0 > table > tbody:nth-child(1) > tr:nth-child(2) > td > span:nth-child(2)').text().split(' ')[0]
        );
        const teacherName = $('#w0 > table > tbody:nth-child(1) > tr:nth-child(7) > th').next().text();

        return {
            code,
            name,
            description,
            facultyName,
            departmentName,
            level,
            year: year === 1 ? 1 : year === 2 ? 2 : year === 3 ? 3 : 4,
            seasons,
            creditsAmount,
            hoursAmount,
            teacherName,
        };
    }
}
