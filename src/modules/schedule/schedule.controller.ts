import {
  BadRequestException,
  Controller,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param,
} from '@nestjs/common';
import { ScheduleService } from './schedule.service';

@Controller('schedule')
export class ScheduleController {
  constructor(protected readonly service: ScheduleService) {}

  @Get(':year/:season')
  public async getScheduleData(
      @Param('year') year: number,
      @Param('season') season: string
  ): Promise<unknown> {
    require('https').globalAgent.options.rejectUnauthorized = false;
    try {
      return await this.service.getScheduleData(year, season);
    } catch (error) {
      // @ts-ignore
      const exception = error?.response?.status;
      if (exception === 404) {
        throw new NotFoundException('Not found');
      } else if (exception === 400) {
        throw new BadRequestException('Inserted values are incorrect');
      } else if (exception === 500) {
        throw new InternalServerErrorException('Failed to connect to server');
      }
    }
  }
}
