import { Injectable } from '@nestjs/common';
import { IScheduleItem } from './types';
import axios from 'axios';
import * as cheerio from 'cheerio';
import { CourseSeason, EducationLevel } from '../../common/types';

@Injectable()
export class ScheduleService {
    async getScheduleData(year: number, season: string): Promise<IScheduleItem[]> {
        const seasonNumber = season === 'autumn' ? 1 : season === 'spring' ? 2 : 3;
        const seasonValue =
            season === 'autumn'
                ? CourseSeason.AUTUMN
                : season === 'spring'
                    ? CourseSeason.SPRING
                    : CourseSeason.SUMMER;
        if (season !== 'autumn') year--;

        const response = await axios.get(
            `https://my.ukma.edu.ua/schedule/?year=${year}&season=${seasonNumber}`
        );
        const $ = cheerio.load(response.data);
        const result: IScheduleItem[] = [];

        const facultiesList = $('#schedule-accordion .panel-info');
        facultiesList.each((_number, element) => {
            const facultyName = $(element)
                .find('.panel-heading > h4 > a')
                .first()
                .text()
                .replace(/[\n]/g, '')
                .trim();

            const parentData = $(element).find('.panel-collapse > div > div').children();
            parentData.each((_number, element) => {
                const levelText = $(element)
                    .find('div > h4 > a')
                    .first()
                    .text()
                    .split(', ')[0]
                    .trim();

                const levelNumber = Number(
                    $(element)
                        .find('div > h4 > a')
                        .first()
                        .text()
                        .split(', ')[1]
                        .trim()[0]
                );

                const level = levelText === 'БП' ? EducationLevel.BACHELOR : EducationLevel.MASTER;
                const childrenData = $(element).find('.panel-collapse > .list-group').children();
                childrenData.each((_number, element) => {
                    const url = String(
                        $(element)
                            .find('div > a')
                            .last()
                            .attr('href')
                    );

                    const specialityName = $(element)
                        .find('div > a')
                        .last()
                        .text()
                        .trim()
                        .split(levelText)[0]
                        .trim();

                    const updateData = $(element)
                        .find('div > span')
                        .first()
                        .text()
                        .split(' ');

                    const date = updateData[1].split('.');
                    const time = updateData[2].slice(0, -1);
                    const updatedAt = `${date[2]}-${date[1]}-${date[0]} ${time}`;

                    result.push({
                        url: url,
                        updatedAt: updatedAt,
                        facultyName: facultyName,
                        specialityName: specialityName,
                        level: level,
                        year: levelNumber === 1 ? 1 : levelNumber === 2 ? 2 : levelNumber === 3 ? 3 : 4,
                        season: seasonValue,
                    });
                });
            });
        });
        return result;
    }
}
